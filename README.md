# OpenML dataset: Shark-Tank-Pitches

https://www.openml.org/d/43396

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Acknowledgement
Thanks to The Concept Center for publishing this article, an analysis, and pulling the data.
Background
Shark Tank is an ABC show where entrepreneurs around the United States can pitch their ideas to billionaires. The show started in Japan in 2009 and has gained traction over the years to what it is today. Wikipedia For this study, I thought that it would be interesting to take the 6 seasons of Shark Tank which consists of 122 episodes and 495 companies, see which ones performed the best and asked the most. In addition, I wanted to see if there are any trends when it comes to those companies getting a deal with at least one shark. Which of the Shark Tank companies will be the best? Lets explore from the data what we can find.
Data
The data was collected from Shark Analytics, who was able to aggregate the information into one relative area.
Date Pulled: 11/28/2017 10:30pm
Total Seasons: 6
Total Episodes: 122
Total Companies on the Show: 495

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43396) of an [OpenML dataset](https://www.openml.org/d/43396). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43396/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43396/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43396/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

